# coke_demo

import os
import sys
import time

if (os.getenv("MODIM_HOME")==None):
    print("Plaese set MODIM_HOME environment variable")
    sys.exit(0)
if (os.getenv("MODIM_IP")==None):
    print("Plaese set MODIM_IP environment variable")
    sys.exit(0)

modimdir = os.getenv('MODIM_HOME')+'/src/GUI'

sys.path.append(modimdir)

from ws_client import *
import ws_client


def imain():

    begin()

    im.setProfile(['*', '*', 'en', '*'])
    im.display.loadUrl('slide.html')
    im.robot.normalPosture()
    time.sleep(1)

    im.execute('party')

    time.sleep(5)

    im.execute('bring_coke')

    time.sleep(5)

    im.execute('give_coke')
    im.robot.setPosture([0.0, -0.21, 1.55, 0.13, -1.24, -0.52, 0.0, 0.50, -0.20, 1.22, 0.60, 0.37])

    time.sleep(5)

    im.execute('party')
    im.robot.normalPosture()

    end()

# give [0.0, -0.21, 1.55, 0.13, -1.24, -0.52, 0.0, 0.50, -0.20, 1.22, 0.60, 0.37]


if __name__ == "__main__":
    cmdsever_ip = os.getenv('MODIM_IP')
    cmdserver_port = 9101
    #demo_ip = '127.0.0.1'
    #demo_port = 8000

    mws = ModimWSClient()
    mws.setCmdServerAddr(cmdsever_ip, cmdserver_port) 
    #mws.setDemoServerAddr(demo_ip, demo_port) 

    mws.run_interaction(imain)


